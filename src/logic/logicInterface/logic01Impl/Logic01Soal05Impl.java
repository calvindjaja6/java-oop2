package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal05Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal05Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int[] deret = new int[this.logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if (i <= 2) deret[i] = 1;
            else deret[i] = deret[i - 1] + deret[i - 2] + deret[i - 3];
            this.logic.array[0][i] = String.valueOf(deret[i]);
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.logic.printSingle();
    }
}
